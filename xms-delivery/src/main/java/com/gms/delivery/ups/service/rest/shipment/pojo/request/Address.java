
package com.gms.delivery.ups.service.rest.shipment.pojo.request;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("AddressLine")
    @Expose
    private List<String> addressLine;
    /*@SerializedName("AddressLine1")
    @Expose
    private String addressLine1;*/
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("StateProvinceCode")
    @Expose
    private String stateProvinceCode;
    @SerializedName("PostalCode")
    @Expose
    private String postalCode;
    @SerializedName("CountryCode")
    @Expose
    private String countryCode;

    public List<String> getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(List<String> addressLine) {
        this.addressLine = addressLine;
    }

    /*public String getAddressLine1() {
        return addressLine1;
    }
    
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }*/
    
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateProvinceCode() {
        return stateProvinceCode;
    }

    public void setStateProvinceCode(String stateProvinceCode) {
        this.stateProvinceCode = stateProvinceCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

}
