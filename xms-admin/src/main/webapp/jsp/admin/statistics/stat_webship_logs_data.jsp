<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib prefix="xms" uri="/xms-tags" %>
<div id="stat_webship_log" style="font-size: 11px; padding: 10px">
    <s:if test="webshipLogs==null || webshipLogs.size()==0">
    </s:if>
    <s:else>
        <s:iterator value="webshipLogs">
            <span><a id="<s:property
						value="airbillNumber" />"
				href="#" onclick="doQuickSearch_new(this.id);">
            <s:property value="logInfo"/></span><br/>
        </s:iterator>
    </s:else>
</div>


<script type="text/javascript">
	function doQuickSearch_new(billNumber) {
		var dialogId = createUniqueId("admin_quick_search", "dialog");
		var quickSearchValue = billNumber;
		console.log(quickSearchValue);
		var quickSearchType = "8";
		var adminQuickSearchDialog;
		var buttons = {};
		buttons["<xms:localization text="Save"/>"] = function () {
            doPostData("history_save.ix?reqType=json", "save-webship-form", "<xms:localization text="Saved Successfully!"/>", "", true);
        };
		buttons["Export PDF"] = function() {
			exportDetail();
		};
		buttons["Close"] = function() {
			$(this).dialog("close");
		}
		var height = $(window).height();
		height = height * 0.80;
		adminQuickSearchDialog = $("<div id='" + dialogId + "'></div>").dialog(
				{
					title : '<xms:localization text="Quick Search" />',
					modal : true,
					autoOpen : false,
					width : "80%",
					maxHeight : height,
					buttons : buttons,
					show : {
						effect : "fade",
						duration : 500
					},
					open : function() {
						$("body").css("overflow", "hidden");
					},
					close : function(e) {
						$("body").removeAttr("style");
						$("#" + dialogId).remove();
					}
				});
		var data = {
			"searchValue" : quickSearchValue,
			"searchType" : quickSearchType
		};
		loadingDialog.dialog("open");
		$
				.post("admin_quick_search.ix?reqType=json", data,
						function(res) {
							loadingDialog.dialog("close");
							if (res.errorCode == "SUCCESS") {
								adminQuickSearchDialog.html(res.content);
								adminQuickSearchDialog.dialog("open");
							} else {
								alertDialog.html(res.errorMsg);
								alertDialog.dialog("open");
							}
						})
				.fail(
						function() {
							alertDialog
									.html('<xms:localization text="System internal error, please contact administrator." />');
							alertDialog.dialog("open");
						});
	}
</script>