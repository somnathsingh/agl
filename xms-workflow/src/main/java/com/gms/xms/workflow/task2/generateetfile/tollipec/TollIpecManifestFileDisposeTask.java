package com.gms.xms.workflow.task2.generateetfile.tollipec;

import com.gms.xms.cache.SystemSettingCache;
import com.gms.xms.common.constants.Attributes;
import com.gms.xms.common.constants.ErrorCode;
import com.gms.xms.common.constants.GenerateETFileConstants;
import com.gms.xms.common.context.ContextBase2;
import com.gms.xms.workflow.core2.Task2;
import com.jcraft.jsch.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.File;
import java.io.FileInputStream;

/**
 * Posted from Sep 26, 2016 10:58:44 AM
 * <p>
 * Author huynd
 */
public class TollIpecManifestFileDisposeTask implements Task2 {
    private static final Log log = LogFactory.getLog(TollIpecManifestFileDisposeTask.class);

    @Override
    public boolean execute(ContextBase2 context) throws Exception {
        try {
            String ftpServer = SystemSettingCache.getInstance().getValueByKey(GenerateETFileConstants.TOLL_IPEC_FTP_SERVER);
            String ftpUserName = SystemSettingCache.getInstance().getValueByKey(GenerateETFileConstants.TOLL_IPEC_FTP_USER_NAME);
            String ftpUserPass = SystemSettingCache.getInstance().getValueByKey(GenerateETFileConstants.TOLL_IPEC_FTP_USER_PASS);
            String serverFolder = SystemSettingCache.getInstance().getValueByKey(GenerateETFileConstants.TOLL_IPEC_SERVER_FOLDER);
            if (!serverFolder.substring(serverFolder.length() - 1).equalsIgnoreCase("/")) {
                serverFolder += "/";
            }
            
            LogFactory.getLog("=====================ftp======"+ftpServer);
            LogFactory.getLog("=====================ftp folder ======"+serverFolder);
            JSch jsch = new JSch();
            Session session = null;
            FTPClient ftpClient = new FTPClient();
            
            try {
            	
            	ftpClient.connect(ftpServer);
            	  
            	// Try to login and return the respective boolean value
            	boolean login = ftpClient.login(ftpUserName, ftpUserPass);
            	if (login) {
            	    System.out.println("Connection established...");
            	    
            	}
            	
                // Connect sftp
               /* session = jsch.getSession(ftpUserName, ftpServer, 21);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setPassword(ftpUserPass);
                session.connect();
                // Open sftp
                Channel channel = session.openChannel("ftp");
                channel.connect();
                ChannelSftp sftpChannel = (ChannelSftp) channel;
                sftpChannel.cd(serverFolder);*/
                String filePath = context.getString(GenerateETFileConstants.FILE_PATH);
                
                System.out.println("=====================ftp file path ======"+filePath);
                
                String fileName = context.getString(GenerateETFileConstants.FILE_NAME);
                filePath = filePath.replace("\\", "/").trim();
                File file = new File(filePath + fileName);
                ftpClient.setFileTransferMode(ftpClient.BINARY_FILE_TYPE);
                ftpClient.enterLocalPassiveMode();
                boolean store = ftpClient.storeFile(serverFolder+fileName, new FileInputStream(file));
                int reply = ftpClient.getReplyCode();
                if(!FTPReply.isPositiveCompletion(reply)) {
                	throw new Exception("Not able to upload the file to toll Ipec FTP server");
                }
                /*sftpChannel.put(new FileInputStream(file), file.getName());
                sftpChannel.exit();
                session.disconnect();*/
                
                context.put(GenerateETFileConstants.CONNECTED_ERROR, "0");
            } catch (Exception e) {
                e.printStackTrace();
                context.put(GenerateETFileConstants.CONNECTED_ERROR, e.getMessage());
            }finally {
            	ftpClient.logout();
            	ftpClient.disconnect();
            }
        } catch (Exception e) {
            context.put(Attributes.ERROR_CODE, ErrorCode.ERROR);
            context.put(Attributes.ERROR_MESSAGE, e.getMessage());
            log.error(e);
            return false;
        }
        return true;
    }

}
