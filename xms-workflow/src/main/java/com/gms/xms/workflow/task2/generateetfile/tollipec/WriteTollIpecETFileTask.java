package com.gms.xms.workflow.task2.generateetfile.tollipec;

import com.gms.xms.common.constants.Attributes;
import com.gms.xms.common.constants.ErrorCode;
import com.gms.xms.common.constants.GenerateETFileConstants;
import com.gms.xms.common.context.ContextBase2;
import com.gms.xms.txndb.vo.generateetfile.GenerateTollETDataVo;
import com.gms.xms.workflow.core2.Task2;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

/**
 * Posted from Sep 26, 2016 11:00:13 AM
 * <p>
 * Author huynd
 */
public class WriteTollIpecETFileTask implements Task2 {
    private static final Log log = LogFactory.getLog(WriteTollIpecETFileTask.class);

    @Override
    public boolean execute(ContextBase2 context) throws Exception {
        try {
            // Create ET file
            String filePath = context.getString(GenerateETFileConstants.FILE_PATH);
            filePath = filePath.replace("\\", "/").trim();
            String fileName = context.getString(GenerateETFileConstants.FILE_NAME);
            DataOutputStream dataOut = new DataOutputStream(new FileOutputStream(filePath + fileName));
            // Write ET file
            String aRow;
            Map<String, String> aMap;
            List<GenerateTollETDataVo> tollETData = context.get(GenerateETFileConstants.GENERATE_TOLL_ET_DATA);
            for (GenerateTollETDataVo tollETDataRow : tollETData) {
                aMap = tollETDataRow.getaRow();
                aRow = "";
                aRow += aMap.get("airbill_number");
                aRow += aMap.get("line_item_no");
                aRow += aMap.get("connote_entered_code");
                aRow += aMap.get("activity_code");
                aRow += aMap.get("segment_code");
                aRow += aMap.get("service_code");
                aRow += aMap.get("system_identifer");
                aRow += aMap.get("despatch_date");
                aRow += aMap.get("reserved_for_futureuse1");
                //aRow += aMap.get("status_flag");
                aRow += aMap.get("receiver_company_name");
                aRow += aMap.get("receiver_address1");
                aRow += aMap.get("receiver_address2");
                aRow += aMap.get("receiver_city_name");
                aRow += aMap.get("receiver_postal_code");
                aRow += aMap.get("delv_early_date");
                aRow += aMap.get("delv_late_date");
                aRow += aMap.get("delv_early_time");
                aRow += aMap.get("delv_late_time");
                aRow += aMap.get("delv_special_instruction");
                aRow += aMap.get("delv_special_code");
                aRow += aMap.get("receiver_contact_name");
                aRow += aMap.get("receiver_phone");
                aRow += aMap.get("receiver_fax");
                aRow += aMap.get("sender_company_name");
                aRow += aMap.get("sender_city_name");
                aRow += aMap.get("sender_postal_code");
                aRow += aMap.get("sender_contact_name");
                aRow += aMap.get("sender_phone");
                aRow += aMap.get("sender_fax");
                aRow += aMap.get("connote_reference");
                aRow += aMap.get("receiver_key");
                //aRow += aMap.get("pallet_chep");
                aRow += aMap.get("reserved_for_futureuse2");
                /*aRow += aMap.get("pallet_loscam");
                aRow += aMap.get("pallet_other");
                aRow += aMap.get("insur_class");
                aRow += aMap.get("insur_cover");
                aRow += aMap.get("charge_code");
                aRow += aMap.get("apply_basic_charge");
                aRow += aMap.get("charge_account");
                aRow += aMap.get("charge_basic");
                aRow += aMap.get("charge_freight");*/
                aRow += aMap.get("charge_extra");
                aRow += aMap.get("charge_code");
                aRow += aMap.get("apply_basic_charge");
                aRow += aMap.get("charge_account");
                aRow += aMap.get("reserved_for_futureuse3");
                aRow += aMap.get("line_reference_2");
                
                /*aRow += aMap.get("charge_other");
                aRow += aMap.get("charge_tax");
                aRow += aMap.get("charge_surcharge");
                aRow += aMap.get("charge_total");
                aRow += aMap.get("charge_insur_per_con");
                aRow += aMap.get("charge_insur_additional");
                aRow += aMap.get("item_identifier");*/
                aRow += aMap.get("line_unit_commod_items_count");
                aRow += aMap.get("reserved_for_futureuse4");
                //aRow += aMap.get("line_unit_commod_items_code");
                aRow += aMap.get("number_of_items");
                aRow += aMap.get("line_weight");
                aRow += aMap.get("line_commod");
                aRow += aMap.get("reserved_for_futureuse5");
                
                //aRow += aMap.get("package_type");
                aRow += aMap.get("desc_of_goods");
                aRow += aMap.get("cubic_length");
                aRow += aMap.get("cubic_width");
                aRow += aMap.get("cubic_height");
                aRow += aMap.get("cubic_qty");
                aRow += aMap.get("cubic_volume");
                aRow += aMap.get("sender_reference");
                aRow += aMap.get("reserved_for_futureuse6");
                
                /*aRow += aMap.get("rating_code");
                aRow += aMap.get("consolidation_flag");
                aRow += aMap.get("security_flag");
                aRow += aMap.get("remote_printing_flag");
                aRow += aMap.get("handpriced_insur");
                aRow += aMap.get("direct_delivery");
                aRow += aMap.get("returns_collections");*/
                aRow += aMap.get("service_description");
                aRow += aMap.get("extra_chrg_codes");
                aRow += aMap.get("reserved_for_futureuse6_5");
                aRow += aMap.get("sender_address1");
                aRow += aMap.get("sender_address2");
                aRow += aMap.get("reserved_for_futureuse7");
                /*aRow += aMap.get("extended_description");
                aRow += aMap.get("sender_address1");
                aRow += aMap.get("sender_address2");
                aRow += aMap.get("connote_narration");
                aRow += aMap.get("sender_location");
                aRow += aMap.get("sender_location_ext");
                aRow += aMap.get("receiver_location");
                aRow += aMap.get("receiver_location_ext");
                aRow += aMap.get("reserved_for_futureuse");*/
                aRow += aMap.get("danger_class");
                aRow += aMap.get("dg_pack_group");
                aRow += aMap.get("dg_un_code");
                aRow += aMap.get("item_sscc");
                aRow += aMap.get("release_date");
                aRow += aMap.get("sms_mobile_number");
                aRow += aMap.get("dp_authentication_number");
                aRow += aMap.get("sms_flag");
                aRow += aMap.get("dp_authentication_flag");
                aRow += aMap.get("send_dpid");
                aRow += aMap.get("delv_dpid");
                aRow += aMap.get("delv_email_address");
                aRow += aMap.get("atl_flag");
                aRow += aMap.get("security_flag");
                aRow += aMap.get("adp_id");
                aRow += aMap.get("business_or_residential_flag");
                aRow += aMap.get("reserved_for_futureuse8");
                aRow += aMap.get("tc_send_id");
                aRow += aMap.get("tc_send_state");
                aRow += aMap.get("tc_delv_state");
                aRow += aMap.get("manifest_number");
                aRow += aMap.get("deleted_connote_flag");
                aRow += aMap.get("reserved_for_futureuse9");
                aRow += aMap.get("source_system");
                aRow += aMap.get("source_system_software_version");
                aRow += aMap.get("reserved_for_futureuse");
                /*aRow += aMap.get("item_number");
                aRow += aMap.get("extended_connote_reference");
                aRow += aMap.get("sender_country_name");
                aRow += aMap.get("receiver_country_name");
                aRow += aMap.get("declared_value");
                aRow += aMap.get("declared_currency_code");
                aRow += aMap.get("other_refs");
                aRow += aMap.get("spare");
                aRow += aMap.get("hud_batch_number");
                aRow += aMap.get("sender_id");
                aRow += aMap.get("sender_state");
                aRow += aMap.get("receiver_state");
                aRow += aMap.get("manifest_number");
                aRow += aMap.get("record_id");
                aRow += aMap.get("system_reserved");
                aRow += aMap.get("checksum");*/
                // aRow += aMap.get("terminator");
                aRow += "\n";
                dataOut.write(aRow.getBytes("UTF-8"));
            }
            dataOut.write("%%EOF\r\n".getBytes("UTF-8"));
            dataOut.close();
        } catch (Exception e) {
            context.put(Attributes.ERROR_CODE, ErrorCode.ERROR);
            context.put(Attributes.ERROR_MESSAGE, e.getMessage());
            log.error(e);
            return false;
        }
        return true;
    }

}
